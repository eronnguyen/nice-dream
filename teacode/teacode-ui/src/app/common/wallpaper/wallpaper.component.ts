import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-wallpaper',
  templateUrl: './wallpaper.component.html',
  styleUrls: ['./wallpaper.component.css']
})
export class WallpaperComponent implements OnInit {

  @Input() flex: string = 'center'
  @Input() backgroundImage: string = ""
  @Input() backgroundColor: string = ""
  @Input() backgroundClip: string = ""
  @Input() backgroundAttachment: string = ""
  @Input() backgroundSize: string = ""
  @Input() backgroundPosition: string = ""
  @Input() color: string = "#000"
  
  constructor() { }

  ngOnInit() {
  }

  justifyContent() {
    if(this.flex == 'center')
      return 'center'
    else if(this.flex == 'left')
      return 'flex-start'
    else (this.flex == 'right')
      return 'flex-end'
  }

  alignItems() {
    if(this.flex == 'center')
      return 'center'
    else if(this.flex == 'left')
      return 'flex-start'
    else (this.flex == 'right')
      return 'flex-start'
  }

  title() {
    return {
      'height': this.flex == "center" ? "50%" : "30%",
      'justify-content': this.justifyContent(),
      'align-items': 'center',
      'padding': '10px',
      'color': this.color
    }
  }

  content() {
    return {
      'height': this.flex == "center" ? "50%" : "70%",
      'justify-content': this.justifyContent(),
      'align-items': this.alignItems(),
      'padding': '10px',
      'color': this.color
    }
  }

  titleStyle() {
    
  }
  contentStyle() {
    
  }

  wall() {
    return {
      'background-color': this.backgroundColor,
      'background-image': "url(" + this.backgroundImage + ")",
      'background-clip': this.backgroundClip,
      'background-size': this.backgroundSize,
      'background-possition': this.backgroundPosition,
      'background-attachment': this.backgroundAttachment,
      'color': this.color
    }
  }
}
