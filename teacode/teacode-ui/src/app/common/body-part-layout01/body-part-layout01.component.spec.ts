import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyPartLayout01Component } from './body-part-layout01.component';

describe('BodyPartLayout01Component', () => {
  let component: BodyPartLayout01Component;
  let fixture: ComponentFixture<BodyPartLayout01Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyPartLayout01Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyPartLayout01Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
