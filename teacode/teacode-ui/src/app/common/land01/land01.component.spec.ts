import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Land01Component } from './land01.component';

describe('Land01Component', () => {
  let component: Land01Component;
  let fixture: ComponentFixture<Land01Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Land01Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Land01Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
