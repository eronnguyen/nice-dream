import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WallpaperLandComponent } from './wallpaper-land.component';

describe('WallpaperLandComponent', () => {
  let component: WallpaperLandComponent;
  let fixture: ComponentFixture<WallpaperLandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WallpaperLandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WallpaperLandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
