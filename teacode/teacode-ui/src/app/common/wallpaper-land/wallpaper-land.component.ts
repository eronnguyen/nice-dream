import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-wallpaper-land',
  templateUrl: './wallpaper-land.component.html',
  styleUrls: ['./wallpaper-land.component.css']
})
export class WallpaperLandComponent implements OnInit {

  @Input() backgroundImage: string = ""
  @Input() backgroundColor: string = ""
  @Input() backgroundClip: string = ""
  @Input() backgroundAttachment: string = ""
  @Input() backgroundSize: string = ""
  @Input() backgroundPosition: string = ""

  constructor() { }

  ngOnInit() {
  }

  wallStyle() {
    return {
      "background-image": "url(" + this.backgroundImage + ")",
      "background-color": this.backgroundColor,
      "background-attachment": this.backgroundAttachment,
      "background-size": this.backgroundSize,
      "background-position": this.backgroundPosition,
      "background-clip": this.backgroundClip
    }
  }
}
