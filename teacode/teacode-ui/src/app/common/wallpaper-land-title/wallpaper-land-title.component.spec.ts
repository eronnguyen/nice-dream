import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WallpaperLandTitleComponent } from './wallpaper-land-title.component';

describe('WallpaperLandTitleComponent', () => {
  let component: WallpaperLandTitleComponent;
  let fixture: ComponentFixture<WallpaperLandTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WallpaperLandTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WallpaperLandTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
