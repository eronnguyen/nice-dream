import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WallpaperLandContentComponent } from './wallpaper-land-content.component';

describe('WallpaperLandContentComponent', () => {
  let component: WallpaperLandContentComponent;
  let fixture: ComponentFixture<WallpaperLandContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WallpaperLandContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WallpaperLandContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
