import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header01',
  templateUrl: './header01.component.html',
  styleUrls: ['./header01.component.css']
})
export class Header01Component implements OnInit {

  @Input() backgroundColor: string = ''
  @Input() textColor: string = ''
  @Input() backgroundImage: string
  @Input() backgroundSize: string
  @Input() absolute: boolean = false
  @Input() absoluteHeight: string
  @Input() repeat: boolean
  @Output() hoverEmit: EventEmitter<any> 
  constructor() { 
    this.hoverEmit = new EventEmitter
  }

  ngOnInit() {
  }

  setStyle() {
    return {
      'background-color': this.backgroundColor,
      'color': this.textColor,
      'background-image': "url("+ this.backgroundImage+")",
      'background-size':  this.backgroundSize,
      'background-repeat': this.repeat ? 'repeat' : 'no-repeat',
      'height': this.absoluteHeight,
      'position': this.absolute ? 'fixed' : 'initial'
    }
  }
}
