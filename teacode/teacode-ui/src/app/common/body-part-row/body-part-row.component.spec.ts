import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyPartRowComponent } from './body-part-row.component';

describe('BodyPartRowComponent', () => {
  let component: BodyPartRowComponent;
  let fixture: ComponentFixture<BodyPartRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyPartRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyPartRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
