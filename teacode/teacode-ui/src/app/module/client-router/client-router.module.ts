import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router';
import { HomeComponent } from '../../page/home/home.component';

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: '', pathMatch: 'full', redirectTo: 'home'},
  {path: '**', redirectTo: 'home'}
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    RouterModule.forRoot(routes, {useHash: true}),
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class ClientRouterModule { }
