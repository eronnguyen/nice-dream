import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import 'hammerjs';
import 'rxjs';

import { AppComponent } from './app.component';
import { MaterialModule } from './module/material/material.module';
import { HomeComponent } from './page/home/home.component';
import { ClientRouterModule } from './module/client-router/client-router.module';
import { UsageModule } from './module/usage/usage.module';
import { Header01Component } from './common/header01/header01.component';
import { Land01Component } from './common/land01/land01.component';
import { BodyPartLayout01Component } from './common/body-part-layout01/body-part-layout01.component';
import { BodyPartRowComponent } from './common/body-part-row/body-part-row.component';
import { WallpaperLandComponent } from './common/wallpaper-land/wallpaper-land.component';
import { WallpaperLandTitleComponent } from './common/wallpaper-land-title/wallpaper-land-title.component';
import { WallpaperLandContentComponent } from './common/wallpaper-land-content/wallpaper-land-content.component';
import { WallpaperComponent } from './common/wallpaper/wallpaper.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    Header01Component,
    Land01Component,
    BodyPartLayout01Component,
    BodyPartRowComponent,
    WallpaperLandComponent,
    WallpaperLandTitleComponent,
    WallpaperLandContentComponent,
    WallpaperComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MaterialModule,
    ClientRouterModule,
    UsageModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
